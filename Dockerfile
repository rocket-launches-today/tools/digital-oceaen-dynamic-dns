FROM node:17.5.0

WORKDIR /srv/ddns

COPY package*.json .

RUN npm install

COPY . .

RUN npm run lint

RUN npm run build

CMD [ "npm", "start" ]
