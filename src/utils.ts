import axios from 'axios';
import db from './db';
import { config } from 'dotenv';
import { WebhookClient, MessageEmbed } from 'discord.js';
import { RowDataPacket } from 'mysql2/promise';
config();

// Discord Webhook
const ID = process.env.DISCORD_HOOK_ID;
const TOKEN = process.env.DISCORD_HOOK_TOKEN;
const client = new WebhookClient({id: `${ID}`, token: `${TOKEN}`});

// Digital Ocean API
const url = `https://api.digitalocean.com/v2/domains/${process.env.DOMAIN}/records/${process.env.DO_DNS_RECORD_ID}`;
axios.defaults.headers.common['Authorization'] = `Bearer ${process.env.DO_TOKEN}`;


export async function getStoredIP() : Promise<string> {
	const database = await db();
	return new Promise((resolve, reject) => {
		// Query the database for the current IP
        interface IP extends RowDataPacket {
            ip: string;
        }
        database.query(`SELECT ${process.env.DB_COLUMN} FROM ${process.env.DB_TABLE}`, [], (err,result:IP[]) => {
        	if(err) {
        		console.log(err);
        		reject(err);
        	}
        	else {
        		// check if the IP is in the database
        		if(Array.isArray(result) && result.length > 0) {
        			resolve(result[0].ip);
        		} else {
        			resolve('');
        		}
        	}
        	database.release();
        });
	});
}

export async function getCurrentIP() : Promise<string> {
	return new Promise((resolve, reject) => {
		// Get the current IP from the ipify API
		axios.get('https://api.ipify.org?format=json')
			.then(response => {
				resolve(response.data.ip);
			})
			.catch(err => {
				reject(err);
			});
	});
}

export async function updateDNS(ip: string) {
	const original = await getStoredIP();
    
	sendDiscordHook(original, ip);

	return new Promise((resolve, reject) => {
		axios.patch(url, {
			data: ip
		})
			.then(async response => {
				await updateDB(ip);
				addLog(original, ip);
				resolve(response.data);
			})
			.catch(err => {
				reject(err);
			});
	});
}

async function addLog(old?:string, new_ip?: string) {
	const database = await db();
	database.query('INSERT INTO dns_update_log (original,new) VALUES (?,?)', [old,new_ip], (err) => {
		if(err) {
			console.log(err);
		}
		database.release();
	});
}

async function sendDiscordHook(old: string, new_ip: string) {
	if(!process.env.DISCORD_HOOK_ID || !process.env.DISCORD_HOOK_TOKEN) {
		return;
	}

	const embed = new MessageEmbed()
		.setTitle('DNS Update')
		.addFields([
			{
				name: 'Was',
				value: old || 'No IP stored',
				inline: true
			},
			{
				name: 'Now',
				value: new_ip,
				inline: true
			}
		])
		.setColor('GREEN')
		.setTimestamp();

	client.send({embeds: [embed], content:`${process.env.DISCORD_USER_ID ? `<@${process.env.DISCORD_USER_ID}>` : 'DNS Update'}`});

}

async function updateDB(ip:string) {
	const database = await db();
	database.query(`UPDATE ${process.env.DB_TABLE} SET ${process.env.DB_COLUMN} = ?`, [ip], (err) => {
		if(err) {
			console.log(err);
		}
		database.release();
	});
}