import * as mysql from 'mysql2';

let pool: mysql.Pool;

export function connect() {
	pool = mysql.createPool({
		host: process.env.DB_HOST,
		user: process.env.DB_USER,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		port: Number(process.env.DB_PORT)
	});

	pool.on('connection', () => {
		console.log('Database connection established');
	});

	return pool;
}

export default function getConnection() : Promise<mysql.PoolConnection> {
	return new Promise((resolve, reject) => {
		if(pool) {
			pool.getConnection((err, connection) => {
				if(err) {
					reject(err);
				}
				else {
					resolve(connection);
				}
			});
		} else {
			const connection = connect();
			connection.getConnection((err, connection) => {
				if(err) {
					reject(err);
				}
				else {
					resolve(connection);
				}
			});
		}
	});
}