import {config} from 'dotenv';
import express from 'express';
import database, { connect } from './db';
import cron from 'node-cron';
import * as utils from './utils';

const app = express();
config();
connect();

cron.schedule('* * * * *', async () => {
	const currentIP = await utils.getCurrentIP();
	const storedIP = await utils.getStoredIP();
	if(currentIP !== storedIP) {
		await utils.updateDNS(currentIP);
	}
});

// routes
app.get('/logs', async(req, res) => {
	if(req.query.api_key !== process.env.API_KEY) {
		res.status(401).json({
			error: 'Unauthorized'
		});
		return;
	}
	const connection = await database();
	connection.query(`SELECT * FROM dns_update_log
    WHERE original IS NOT NULL AND original != new
    ORDER BY updatedAt DESC;`, [], (err, result) => {
		if(err) {
			res.status(500).send(err);
		}
		else {
			res.send(result);
		}
		connection.release();
	});
});

app.get('/manual-update', async(req, res) => {
	if(req.query.api_key !== process.env.API_KEY) {
		res.status(401).json({
			error: 'Unauthorized'
		});
		return;
	}
	utils.updateDNS(await utils.getCurrentIP()).then(result => {
		res.send(result);
	}).catch(err => {
		res.status(500).send(err);
	});
});

app.listen(3000, async() => {
	console.log('Server started on port 3000');
});